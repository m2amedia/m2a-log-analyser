"""Tool for analysing origin cache logs."""

import abc
import collections
import datetime
import enum
import functools
import fractions
import ipaddress
import logging
import os
import pathlib
import urllib.parse
import random
import re
import statistics
import sys

import click
import dateutil.parser
import isodate
import pyparsing

import m2a_log_analyser
import m2a_log_analyser.column
import m2a_log_analyser.output
import m2a_log_analyser.record
import m2a_log_analyser.query
import m2a_log_analyser.source


log = logging.getLogger(__name__)
REGEX_ACCESS = re.compile("^(?P<client>(\d{1,3}\.){3}\d{1,3})\s(?P<unknown1>.+?)\s(?P<unknown2>.+?)\s\[(?P<time>.+)\]\s\"(?P<request>.+?)\"\s(?P<status>[1-5]\d{2})\s(?P<length>\d+)\s\".*?\"\s\"(?P<agent>.*?)\"\s\"(?P<upstreams>.*)\"(\s(?P<response>\d+.\d+)\s(?P<upstream_time>-|\d+.\d+))?")


class HTTPStatusError(Exception):
    """Raised for invalid HTTP status codes."""


class BadRecordError(Exception):
    """Raised when a raw log record cannot be parsed."""

    def __init__(self, record, reason="unknown"):
        self.record = record
        self.reason = reason

    def __str__(self):
        return str(self.reason)


class QueryError(Exception):
    """Raised for invalid queries."""


class Path(click.Path):

    def convert(self, value, parameter, context):
        converted = super().convert(value, parameter, context)
        if self.allow_dash and converted == "-":
            converted = "/proc/{pid}/fd/0".format(pid=os.getpid())
        return pathlib.Path(converted)


def _parse(serialised):
    """Parse a serialised record."""
    match = REGEX_ACCESS.match(serialised)
    if not match:
        raise BadRecordError(serialised, "regex mismatch")
    request = match.group("request")
    try:
        request_method, request_tail = request.split(" ", 1)
        request_path, _ = request_tail.rsplit(" ", 1)
        request_method = m2a_log_analyser.record.HTTPMethod(request_method)
    except ValueError as error:
        raise BadRecordError(
            serialised, "cannot parse HTTP request: " + str(error)) from error
    url = urllib.parse.urlparse(request_path)
    url_path = urllib.parse.unquote(url.path)
    url_query = urllib.parse.parse_qs(url.query)
    response_status = m2a_log_analyser.record.HTTPStatus(match.group("status"))
    response_length = int(match.group("length"))
    time_response = None
    time_upstream = None
    if match.group("response"):
        time_response = float(match.group("response"))
    if match.group("upstream_time"):
        try:
            time_upstream = float(match.group("upstream_time"))
        except ValueError:
            pass
    return m2a_log_analyser.record.Record(
        time=dateutil.parser.parse(match.group("time"), fuzzy=True),
        client=ipaddress.ip_address(match.group("client")),
        client_agent=match.group("agent"),
        method=request_method,
        path=url_path,
        query=url_query,
        status=response_status,
        length=response_length,
        time_response=time_response,
        time_upstream=time_upstream,
    )


def _parse_source(source):
    """Parse a stream of raw records.

    This parses an iterable of raw records from a source into a stream
    of parsed records. The record error value is preserved and
    propagated.

    :param source: Source of raw records.
    :type source: m2a_log_analyser.source.Source

    :returns: Stream of parsed records.
    :rtype: Iterable[Tuple[float, m2a_log_analyser.record.Record]]
    """
    for record_error, record_raw in source:
        try:
            yield record_error, _parse(record_raw)
        except BadRecordError as error:
            log.error("Skipping bad record: %s", error.reason)


def _parse_source_socket(source):
    """Parse a stream of MessagePack log records."""
    for _, (error, record) in source:
        yield error, record  # ew


def _validate_columns(_1, _2, values):
    columns = []
    for column_id in values:
        try:
            columns.append(m2a_log_analyser.column.Column.parse(column_id))
        except m2a_log_analyser.column.ColumnError as error:
            raise click.BadParameter(str(error))
    return columns


@click.command()
@click.version_option(m2a_log_analyser.version)
@click.argument(
    "path",
    type=Path(allow_dash=True),
)
@click.option(
    "--watch",
    is_flag=True,
    help="Watch log files being actively written to.",
)
@click.option(
    "--log-level",
    type=click.Choice([logging.getLevelName(level).lower() for level in [
        logging.DEBUG,
        logging.INFO,
        logging.WARNING,
        logging.ERROR,
        logging.CRITICAL,
    ]]),
    default="info",
    show_default=True,
    help="Only show logs at or above this level.",
)
@click.option(
    "--time-start",
    default="2000-01-01T00:00:00Z",
    type=isodate.parse_datetime,
)
@click.option(
    "--time-end",
    default="3000-01-01T00:00:00Z",
    type=isodate.parse_datetime,
)
@click.option(
    "-p",
    "--period",
    default="1/15",
    type=fractions.Fraction,
    help="Periodicity of the statistics in samples per second.",
)
@click.option(
    "-n",
    "--sample",
    default="1/1",
    type=fractions.Fraction,
    help="Proportion of records to sample per period.",
)
@click.option(
    "-q",
    "--query",
    default="(all AND all)",
    type=m2a_log_analyser.query.Query.from_string,
    help="Select which log records are considered.",
)
@click.option(
    "-c",
    "--column",
    "columns",
    multiple=True,
    callback=_validate_columns,
    help="Statistic to output for each period.",
)
@click.option(
    "-o",
    "--out",
    default="table",
    type=click.Choice(["table", "top", "csv", "socket"]),
    help="Output style for statistics.",
)
@click.option(
    "--out-socket-path",
    default=None,
    type=pathlib.Path,
    help="Path to the records socket.",
)
def _main(*, path, watch, log_level, time_start,
          time_end, period, sample, query, columns, out, **out_options):
    logging.basicConfig(
        level=log_level.upper(),
        format="%(asctime)s %(levelname)s %(message)s",
        stream=sys.stderr,
    )
    output_options = {}
    output_option_prefix = "out_" + out + "_"
    for option_name, option_value in out_options.items():
        if option_name.startswith(output_option_prefix):
            output_option = option_name[len(output_option_prefix):]
            output_options[output_option] = option_value
    output = {
        "csv": m2a_log_analyser.output.CSVOutput,
        "table": m2a_log_analyser.output.TableOutput,
        "top": m2a_log_analyser.output.TopOutput,
        "socket": m2a_log_analyser.output.SocketOutput,
    }[out](columns, **output_options)
    period = datetime.timedelta(seconds=float(1 / period))
    source = m2a_log_analyser.source.PathSource(path)
    if watch:
        source = m2a_log_analyser.source.WatchPathSource(path)
    source_parse = _parse_source
    if path.is_socket():
        source = m2a_log_analyser.source.SocketSource(path, query)
        source_parse = _parse_source_socket
        query = m2a_log_analyser.query.Query.from_string("(all)")
    source.sample = sample
    with source, output:
        records = source_parse(source)
        records_filtered = ((error, record) for error, record
                            in records if query.evaluate(record))
        for record_error, record_set in _bin(
                time_start, time_end, records_filtered, period):
            output.publish(record_set, record_error)


def _bin(time_start, time_end, records, period):
    """Group records into record sets.

    This will group a stream of parsed records into a stream of time
    bin record sets. Each record set will span the given duration.

    Additionally, bins are only generated for those in the given time
    range. All records outside of this range are ignored.

    The record error for the last record in a set is what is returned
    along with the set. It is safe to change the sampling rate of the
    source during iteration so long as it's from the same thread.

    :param time_start: Start time of records.
    :type time_start: datetime.datetime
    :param time_end: End time of records.
    :type time_end: datetime.datetime
    :param records: Stream of parsed records.
    :type records: Iterable[Tuple[float, m2a_log_analyser.record.Record]]
    :param period: Periodicity of the the time bins.
    :type period: datetime.duration

    :returns: Stream of record sets and error.
    :rtype: Iterable[Tuple[float, m2a_log_analyser.record.RecordSet]]
    """
    record_set = None
    record_error = 1.0
    for record_error, record in records:
        if record.time >= time_start and record.time < time_end:
            if not record_set:
                record_set = m2a_log_analyser.record.RecordSet(
                    record.time, record.time + period)
            record_set.add(record)
            if record_set.complete():
                yield record_error, record_set
                record_set = record_set.next()
    if record_set:
        yield record_error, record_set


if __name__ == "__main__":
    _main()
