"""Record filtering queries."""

import enum
import logging

import pyparsing

import m2a_log_analyser.category


log = logging.getLogger(__name__)


class QueryError(Exception):
    """Raised when parsing invalid queries."""


class Query:

    _CATEGORIES = [
        m2a_log_analyser.category.AllCategory,
        m2a_log_analyser.category.CIDRCategory,
        m2a_log_analyser.category.EventCategory,
        m2a_log_analyser.category.ProfileCategory,
        m2a_log_analyser.category.ProtocolCategory,
        m2a_log_analyser.category.ResourceCategory,
    ]

    class Operator(enum.Enum):

        AND = "AND"
        OR = "OR"
        NOT = "NOT"

    def __init__(self, left, operator, right):
        self.left = left
        self.right = right
        self.operator = operator

    def __str__(self):
        return "({0.left} {0.operator.value} {0.right})".format(self)

    @classmethod
    def parse(cls, tokens):
        if len(tokens) == 1:
            tokens = ["all", cls.Operator.AND.value, tokens[0]]
        if len(tokens) == 2 and tokens[0] == cls.Operator.NOT.value:
                tokens = ["all", cls.Operator.NOT.value, tokens[1]]
        if len(tokens) != 3:
            raise QueryError(
                "Expressions must have an operator and two operands")
        token_left, token_operand, token_right = tokens
        operator = cls.Operator(token_operand)
        left = cls._parse_operand(token_left)
        right = cls._parse_operand(token_right)
        return cls(left, operator, right)

    @classmethod
    def from_string(cls, string):
        try:
            query_tokens = pyparsing.nestedExpr().parseString(string).asList()
        except pyparsing.ParseException as error:
            raise QueryError(str(error)) from error
        query = cls.parse(query_tokens[0])
        log.info("Canonical query {}".format(query))
        return query

    @classmethod
    def _parse_operand(cls, operator):
        if isinstance(operator, str):
            return cls._parse_category(operator)
        return cls.parse(operator)

    @classmethod
    def _parse_category(cls, category_string):
        for category in cls._CATEGORIES:
            category_instance = category.from_string(category_string)
            if category_instance is not None:
                return category_instance
        raise QueryError("Unknown category {!r}".format(category_string))

    def _evaluate_operand(self, operand, record):
        if isinstance(operand, Query):
            return operand.evaluate(record)
        return record in operand

    def evaluate(self, record):
        left = self._evaluate_operand(self.left, record)
        right = self._evaluate_operand(self.right, record)
        if self.operator is self.Operator.AND:
            return left and right
        elif self.operator is self.Operator.OR:
            return left or right
        elif self.operator is self.Operator.NOT:
            return left and not right
