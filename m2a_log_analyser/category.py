"""Categorise and classify log records."""

import abc
import enum
import re


class Category(abc.ABC):
    """Category of log records.

    Categories are identifiers that can be used to group similar
    records together. These categories can then be used in queries to
    filter record sets. The semantics of whether a record is in a
    category is up to the concrete category implementation.

    Each category should implement its own parser. Arbitrary data can
    be encoded into the category ID itself, which is used to control
    how the category selects records.  See :class:`CIDRCategory`.

    Sub-classes must implement :meth:`__str__` which returns the
    canonical form of the category ID. Categories are cacheable on
    the canonical form.

    Category membership is determined on-demand. See
    :meth:`__contains__`.
    """

    @abc.abstractmethod
    def __str__(self):
        """Canonical form of the category."""
        raise NotImplementedError

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        if isinstance(other, Category):
            return str(self) == str(other)
        return NotImplemented

    @abc.abstractmethod
    def __contains__(self, record):
        """Check if a record is in the category.

        :param record: Record to check if a member of the category.
        :type record: m2a_log_analyser.record.Record

        :returns: True if the record is in the category, false
            otherwise.
        :rtype: bool
        """
        raise NotImplementedError

    @classmethod
    @abc.abstractmethod
    def from_string(self, string):
        """Parse a category from a string.

        :param string: String to parse the category from.
        :type string: str

        :returns: Category object if any could be parsed from the
            given category ID.
        :rtype: Optional[Category]
        """
        raise NotImplementedError


class AllCategory(Category):
    """Selects all records."""

    def __str__(self):
        return "all"

    @classmethod
    def from_string(cls, string):
        if string == "all":
            return cls()

    def __contains__(self, record):
        return True


class EventCategory(Category):
    """Select records by event ID.

    .. code-block::

        event/<event-id>
        event/11kkmyk67ovdu1mlmjjeobstam

    The ID of the event is encoded in the requested path as the
    first element. The exact event ID must be given as part of the
    category.
    """

    def __init__(self, event):
        self.event = event

    def __str__(self):
        return "event/" + self.event

    @classmethod
    def from_string(cls, string):
        if string.startswith("event/"):
            profile = string[len("event/"):]
            if profile:
                return cls(profile)

    def __contains__(self, record):
        if record.path:
            parts = record.path.split("/")
            return len(parts) >= 2 and self.event == parts[1]


class CIDRCategory(Category):
    """Select records by client CIDR block.

    .. code-block::

        client/cidr/<cidr>
        client/cidr/104.86.110.61/0
        client/cir/10.0.0.0/8

    This category checks if the record's requesting client address is
    in the given CIDR block. As such, this can also be used to match
    the exact client address as well.
    """

    _PATTERN = re.compile(r"^client/cidr/(.+)$")

    def __init__(self, network):
        self.network = network

    def __str__(self):
        return "client/cidr/" + str(self.network)

    @classmethod
    def from_string(cls, string):
        match = cls._PATTERN.match(string)
        if match:
            try:
                return cls(ipaddress.ip_network(match.groups(0)[0]))
            except ValueError:
                pass

    def __contains__(self, record):
        return record.client and record.client in self.network


class ResourceCategory(Category):
    """Select records by type of resource.

    .. code-block::

        resource/manifest
        resource/video
        resource/audio

    How each kind of resources depends on the protocol used by the
    request. For HLS and Smooth, audio and video are distinct resources
    but for DASH both are included in a single response. Therefore,
    DASH requests for audio resources will also match video and vice
    versa.
    """

    class Resource(enum.Enum):

        ALL = "all"
        MANIFEST = "manifest"
        VIDEO = "video"
        AUDIO = "audio"

    def __init__(self, type_):
        self.type = type_
        self._category_dash = ProtocolCategory.from_string("protocol/dash")
        self._category_hls = ProtocolCategory.from_string("protocol/hls")
        self._category_hss = ProtocolCategory.from_string("protocol/hss")

    def __str__(self):
        return "resource/" + self.type.value

    @classmethod
    def from_string(cls, string):
        if string.startswith("resource/"):
            string_type = string[len("resource/"):]
            type_ = cls.Resource.ALL
            if string_type:
                try:
                    type_ = cls.Resource(string_type)
                except ValueError:
                    return
            return cls(type_)

    def __contains__(self, record):
        types = [self.type]
        if self.type is self.Resource.ALL:
            types = [self.Resource.MANIFEST,
                     self.Resource.AUDIO, self.Resource.VIDEO]
        resources = []
        if record in self._category_dash:
            resources.extend(self._identifity_resource_dash(record))
        elif record in self._category_hls:
            resources.extend(self._identifity_resource_hls(record))
        elif record in self._category_hss:
            resources.extend(self._identifity_resource_hss(record))
        return any(resource in types for resource in resources)

    def _identifity_resource_dash(self, record):
        if record.path:
            if ".mpd" in record.path:
                return [self.Resource.MANIFEST]
            elif ".dash" in record.path:
                return [self.Resource.AUDIO, self.Resource.VIDEO]
        return []

    def _identifity_resource_hls(self, record):
        if record.path:
            if ".m3u8" in record.path:
                return [self.Resource.MANIFEST]
            elif ".ts" in record.path:
                return [self.Resource.VIDEO]
            elif ".aac" in record.path:
                return [self.Resource.AUDIO]
        return []

    def _identifity_resource_hss(self, record):
        if record.path:
            if "/Manifest" in record.path:
                return [self.Resource.MANIFEST]
            elif ("QualityLevels" in record.path
                    and "Fragments(video=" in record.path):
                return [self.Resource.VIDEO]
            elif ("QualityLevels" in record.path
                    and "FragmentInfo(audio" in record.path):
                return [self.Resource.AUDIO]
        return []


class ProtocolCategory(Category):
    """Select records by streaming protocol.

    .. code-block:: text

        protocol/dash
        protocol/hls
        protocol/hss

    The protocol is determined by scaning the requested path for
    manifest and fragment extensions.
    """

    class Protocol(enum.Enum):

        DASH = "dash"
        HLS = "hls"
        HSS = "hss"

    _PATTERN = re.compile(r"^protocol/(.+)$")

    def __init__(self, protocol):
        self.protocol = protocol

    def __str__(self):
        return "protocol/" + self.protocol.value

    @classmethod
    def from_string(cls, string):
        match = cls._PATTERN.match(string)
        if match:
            try:
                return cls(cls.Protocol(match.groups(0)[0]))
            except ValueError as error:
                pass

    def __contains__(self, record):
        if self.protocol is self.Protocol.DASH:
            return record.path and (
                ".dash" in record.path or ".mpd" in record.path)
        if self.protocol is self.Protocol.HLS:
            return record.path and (
                ".m3u8" in record.path
                or ".ts" in record.path
                or ".aac" in record.path
            )
        if self.protocol is self.Protocol.HSS:
            return record.path and (
                "/Manifest" in record.path or "QualityLevels" in record.path)


class ProfileCategory(Category):
    """Select records by USP profile.

    .. code-block:: text

        profile/<usp-profile-name>
        profile/all
        profile/tv
        profile/mob

    The name of the USP profile is encoded in the requested path,
    as the second element, after the event ID. Possible values depend
    on the USP configuration, but typically things like "all", "tv"
    and "mob" are used.

    For Smooth streams, they USP profile in the path will have a
    ".ismv" suffix which is ignored by this category.
    """

    def __init__(self, profile):
        self.profile = profile

    def __str__(self):
        return "profile/" + self.profile

    @classmethod
    def from_string(cls, string):
        if string.startswith("profile/"):
            profile = string[len("profile/"):]
            if profile:
                return cls(profile)

    def __contains__(self, record):
        if record.path:
            parts = record.path.split("/")
            if len(parts) >= 3:
                return parts[2].split(".")[0] == self.profile
