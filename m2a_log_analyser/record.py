"""Generic representations for HTTP logs."""

import collections
import enum
import functools
import logging


log = logging.getLogger(__name__)


class HTTPStatusError(Exception):
    """Raised for invalid HTTP status codes."""


Record = collections.namedtuple("Record", (
    "time",
    "client",
    "client_agent",
    "path",
    "query",
    "method",
    "status",
    "length",
    "time_response",
    "time_upstream",
))
Record.__doc__ = """\
HTTP request as read from an HTTP's server access log.

All fields are optional except for :attr:`time`.

:ivar time: Time the request was logged as naive, UTC time.
:vartype time: datetime.datetime
:ivar client: Address of the requesting client.
:vartype client: Optional[ipaddress.IPv4Address]
:ivar client_agent: User agent of the requesting client, which will
    not be empty if given.
:vartype client_agent: Optional[str]
:ivar path: Unescaped URL path requested.
:vartype path: Optional[str]
:ivar query: Form decoded URL query string.
:vartype query: Optional[multidict.MultiDict]
:ivar method: HTTP method of the request.
:vartype method: Optional[HTTPMethod]
:ivar status: HTTP status of the response.
:vartype status: Optional[HTTPStatus]
:ivar length: Content length of the response in bytes.
:vartype length: Optional[int]
:ivar time_response: Time taken to process request.
:vartype time_response: Optional[datetime.timedelta]
:ivar time_upstream: Time taken to perform any upstream requests.
:vartype time_upstream: Optional[datetime.timedelta]
"""


class RecordSet:
    """Chronologically sorted set of records.

    Record sets are used to sort log records into fixed-duration bins.
    Each set has a start and end time. If a record is added to a set
    that extends beyond the set, it will overflow into the next set
    created by :meth:`next`.

    Records sets enforce strict chronological ordering of records
    by discarding out-of-order records.

    Records within the set can be accessed through iteration.
    """

    def __init__(self, start, end):
        self.start = start
        self.end = end
        self._time = self.start
        self._records = []
        self._overflow = []

    def __len__(self):
        return len(self._records)

    def __iter__(self):
        yield from self._records

    @property
    def duration(self):
        """Duration of the record set."""
        return self.end - self.start

    def add(self, record):
        """Add a record to the set.

        If the record occurs after the record set's time range, then
        set will be marked as complete. The given record will overflow
        into the next record set returned by :meth:`next`.

        If the record precedes the record set's time range or the last
        record in the set, then it will be discarded and an error
        logged.
        """
        if record.time < self._time:
            log.error("Record out of order, expected ≥ %s but "
                      " got %s; discarding record", self._time, record.time)
            return
        if record.time > self.end:
            self._overflow.append(record)
        else:
            self._records.append(record)
            self._time = record.time

    def complete(self):
        """Check whether the set is complete.

        A record set is complete if attempts to add records beyond
        the end time of the set have been made. When a record set is
        complete, :meth:`next` should be called to get the next,
        chronological set.

        :returns: Whether the set is complete or not.
        :rtype: bool
        """
        return bool(self._overflow)

    def next(self):
        """Get the next chronological record set.

        If the record set is complete, this will create a new
        chronologically adjacent set with same duration. Any records
        that overflowed the current set will be added to the new one.

        :returns: New record set.
        :rtype: RecordSet
        """
        record_set = RecordSet(self.end, self.end + self.duration)
        for record_overflow in self._overflow:
            record_set.add(record_overflow)
        return record_set


class HTTPMethod(enum.Enum):
    """Enumeration of possible HTTP methods."""

    # HTTP
    GET = "GET"
    HEAD = "HEAD"
    OPTIONS = "OPTIONS"
    POST = "POST"
    PUT = "PUT"

    # WebDAV
    PROPFIND = "PROPFIND"


@functools.total_ordering
class HTTPStatus:
    """HTTP status code.

    Statuses are orderable and hashable. Unlike :data:`http.HTTPStatus`
    this class supports arbitrary status codes within the 100 to 599
    range. As a consequence of this, not all status codes will have
    descriptions.

    :param status: Status code.
    :type status: Union[str, int]

    :raises HTTPStatusError: If the given status code cannot be
        converted to a canonical integer representation or if its
        out of range of the legal values.
    """

    class Category(enum.IntEnum):
        """HTTP status code general category.

        The names are taken from RFC 7231's list of possible status
        code classes.
        """

        INFORMATIONAL = 100
        SUCCESSFUL = 200
        REDIRECTION = 300
        CLIENT_ERROR = 400
        SERVER_ERROR = 500

    def __init__(self, status):
        try:
            status_numeric = int(status)
        except (ValueError, TypeError) as error:
            raise HTTPStatusError from error
        if status_numeric < 100 or status_numeric > 599:
            raise HTTPStatusError
        self._status = status_numeric

    @property
    def description(self):
        """Human readable description for the status."""
        raise NotImplementedError  # TODO

    @property
    def category(self):
        """General category of the status code."""
        return HTTPStatus.Category(100 * (int(self) // 100))

    def __int__(self):
        return self._status

    def __str__(self):
        return str(self._status)

    def __eq__(self, other):
        return isinstance(other, HTTPStatus) and int(other) == int(self)

    def __hash__(self):
        return hash(str(self))

    def __lt__(self, other):
        return isinstance(other, HTTPStatus) and int(other) < int(self)
