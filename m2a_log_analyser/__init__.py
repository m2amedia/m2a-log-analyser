"""M2A (HTTP) Log Analyser."""

import email

import multidict
import pkg_resources


#: Package metadata as loaded from PKG-INFO.
metadata = multidict.CIMultiDict(
    email.message_from_string(
        pkg_resources.get_distribution(__package__).get_metadata("PKG-INFO")),
)


#: Package version string.
version = metadata.getone("Version")


#: Package version tuple containing the major, minor and patch versions.
version_info = tuple(int(part) for part in version.split("."))
