"""Sources for log records."""

import abc
import datetime
import ipaddress
import logging
import pathlib
import socket
import random

import dateutil.tz
import inotify.adapters
import msgpack

import m2a_log_analyser.record


log = logging.getLogger(__name__)


class Source(abc.ABC):
    """Source of raw records.

    This base class provides a framework for building sources of
    raw HTTP log records. A raw record refers to a log record *before*
    it has been deserialised, e.g. by parsing a line of text.

    Sources are iterable. Raw records are read from the source and
    then subjected to random sampling before they're returned as part
    of the iterable.

    Sampling of the source is controlled via the :attr:`sample`
    attribute, which is a number between zero and one representing
    the proportion of raw records to return.

    Sources are used as context managers. A source's context must be
    entered before iteration is possible. Concrete implementations
    may should use this manage the life cycle of resources they depend
    on.
    """

    def __init__(self):
        self._sample = 1.0
        self._context = False

    def __enter__(self):
        self._context = True
        return self

    def __exit__(self, error, type_, traceback):
        self._context = False

    def __iter__(self):
        if not self._context:
            raise RuntimeError("Source context not entered")
        return self

    def __next__(self):
        record_raw = ...
        while record_raw is not None:
            record_raw = self.next()
            record_sample = random.random() <= self._sample
            if record_raw is not None and record_sample:
                return float(1 / self._sample), record_raw
        raise StopIteration

    @property
    def sample(self):
        """Get the configured sampling rate."""
        return self._sample

    @sample.setter
    def sample(self, rate):
        """Set the sampling rate.

        The given sampling rate will be clamped to the [0, 1] range
        before being applied to the source.

        :param rate: Sampling rate to set for the source.
        :type rate: Union[float, int, fractions.Fraction]
        """
        rate_numeric = float(rate)
        self._sample = min(1, max(0, rate_numeric))
        log.debug("{0} sampling rate set to {0._sample}".format(self))

    @abc.abstractmethod
    def next(self):
        """Get next record from the source.

        If the end of the source has been reached, ``None`` will be
        returned instead of a serialised record.

        .. note::
            This should not be called directly. Raw records should be
            accessed via the iterable interface only.

        :returns: The next serialised record from the source.
        :rtype: Optional[Some]
        """
        raise NotImplementedError


class PathSource(Source):
    """Read records from a file specified by path.

    This will read log records from a file specified by a path until
    the end of the file is reached. Once the end of file is reached,
    the source is exhausted and will terminate.

    :param path: Path of the file to read from.
    :type path: pathlib.Path
    """

    def __init__(self, path):
        super().__init__()
        self._path = path
        self._file = None

    def __enter__(self):
        super().__enter__()
        log.info("Reading logs from %s", self._path)
        self._file = self._path.open()
        return self

    def __exit__(self, error, type_, traceback):
        super().__exit__(error, type_, traceback)
        if self._file is not None:
            self._file.close()
        self._file = None

    def next(self):
        """Read a line from the file.

        Whitespace is stripped from the end of the lines.

        :returns: Line read from the file, or ``None`` if the end
            of the file has been reached.
        """
        return self._file.readline().rstrip() or None


class WatchPathSource(Source):
    """Watch a path for files changes.

    Like, :class:`PathSource`, this read log records from a file
    specified by a path. However, unlike regular path sources, this
    will wait when the end of file has been reached, making it suitable
    for analysing logs that are being actively written to.

    Moreover, should the file specified by the path be recreated,
    the source will open a handle to the new file. When the previous
    file has been exhausted, log records will be read from the new
    file -- closing the old file in the process.

    This means that this source is suitable for reading log files
    that are rotated, so long as the rotated file always has the same
    name.

    :param path: Path of the file to read from.
    :type path: pathlib.Path
    """

    _IN_CREATE = "IN_CREATE"

    def __init__(self, path):
        super().__init__()
        self._path = path
        self._notifier = inotify.adapters.Inotify()
        self._notifier.add_watch(str(self._path.parent))
        self._notifications = self._notifier.event_gen(yield_nones=False)
        self._files = []

    def __enter__(self):
        super().__enter__()
        self._open()

    def __exit__(self, error, type_, traceback):
        super().__exit__(error, type_, traceback)
        self._notifier.remove_watch(str(self._path.parent))
        for file_ in self._files:
            file_.close()
        self._files.clear()

    def next(self):
        """Read next line and process events.

        This will attempt to read the next line from the currently
        open file. Failing that, file system events will be processed
        until a new file can be opened or a line is writtent to the
        existing file.

        .. note::
            Due to the nature of this source, this will never return
            ``None`` and therefore the source can never be exhausted.
        """
        # If it's possible, we should filter inotify events lower
        # down to avoid potentially building large event buffers
        # whilst reading from busy log files.
        line = self._read()
        while not line:
            _, event_names, directory, name = next(self._notifications)
            event_path = pathlib.Path(directory) / name
            if event_path == self._path:
                if self._IN_CREATE in event_names:
                    log.info("File rotated; opening new file")
                    self._open()
            line = self._read()
        return line

    def _read(self):
        """Read a line from the currently file.

        If EOF is found and there's a second file waiting in to be
        read, the current file is closed. Subsequent calls will read
        from the second file. Note, when this happens, an empty byte
        string is returned.

        Whitespace is stripped from the end of the lines.

        :returns: A line read from the current file or an empty line
            if cycling to a new file.
        :rtype: bytes
        """
        line = self._files[0].readline().rstrip()
        if not line and len(self._files) > 1:
            log.info("EOF reached; closing old descriptor")
            self._files[0].close()
            self._files.pop(0)
            return b""
        return line

    def _open(self):
        """Open a file for reading.

        If the file was successfully opened, it is enqueued so that
        it may be read with subsequent :meth:`read` invocations.

        If the file cannot be opened, then this does nothing.
        """
        try:
            self._files.append(self._path.open())
            log.info("Opened %s for reading", self._path)
        except FileNotFoundError as error:
            log.error("Could not open file: %s", error)


class SocketSource(Source):
    """Read records from another log analyser's socket.

    When the source's context is entered, a connection to the configured
    socket path is made. Once a connection has been made the desired
    record filter query is sent across the socket to begin receiving
    records.

    :param path: Path to the socket to connect to.
    :type path: pathlib.Path
    :param query: Query to select which records are received.
    :type query: m2a_log_analyser.query.Query
    """

    def __init__(self, path, query):
        super().__init__()
        self._query = query
        self._path = path
        self._buffer = msgpack.Unpacker(use_list=False, encoding="utf-8")
        self._socket = None

    def __enter__(self):
        super().__enter__()
        log.debug("Connecting to %s", self._path)
        try:
            self._socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            self._socket.connect(str(self._path))
            log.debug("Connected; sending query %s", self._query)
            query_serialised = msgpack.packb(
                str(self._query), use_bin_type=True)
            self._socket.sendall(query_serialised)
        except socket.error as error:
            log.error("Could not connect: %s", error)


    def __exit__(self, error, type_, traceback):
        self._socket.close()

    def next(self):
        """Decode a single message.

        This will attempt to decode a single MessagePack serialised
        message from the internal buffer. If no message can be decoded,
        a recv(2) is made to populate the buffer.
        """
        deserialised = None
        while deserialised is None:
            try:
                deserialised = self._buffer.unpack()
            except msgpack.OutOfData:
                received = b""
                try:
                    received = self._socket.recv(4096)
                except socket.error as error:
                    log.critical("Error reading from socket: %s", error)
                self._buffer.feed(received)
                if not received:
                    log.debug("Connection lost")
                    return
        return self._unpack(deserialised)

    def _unpack(self, deserialised):
        """Unpack a deserialised log record structure.

        This is takes a deserialised log record as generated by the
        socket output and uses it to re-construct the canonical form
        of the record.

        :returns: Record error and canonical record.
        :rtype: Tuple[float, m2a_log_analyser.record.Record]
        """
        record_error, record_structure = deserialised
        record_time = datetime.datetime.fromtimestamp(
            record_structure[0], dateutil.tz.UTC)
        record_client = (ipaddress.ip_address(record_structure[1])
                         if record_structure[1] is not None else None)
        record_method = (m2a_log_analyser.record.HTTPMethod(record_structure[5])
                         if record_structure[5] is not None else None)
        record_status = (m2a_log_analyser.record.HTTPStatus(record_structure[6])
                         if record_structure[6] is not None else None)
        record = m2a_log_analyser.record.Record(
            record_time,
            record_client,
            record_structure[2],
            record_structure[3],
            record_structure[4],
            record_method,
            record_status,
            record_structure[7],
            record_structure[8],
            record_structure[9],
        )
        return record_error, record
