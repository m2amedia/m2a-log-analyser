"""Summarise record sets.

Before a record set is emitted by an output, it is summarised into
a set of columns. Columns take a record set and reduce it down to
a single value.

Each column is identified by an ID. IDs may be static strings or,
in the case of ranked columns for example, may encode additional
information into the ID itself, giving an unbounded column ID space.

Various columns are generalised into the four concrete :class:`Column`
implementations below. This gives consistent ID structure for similar
columns.
"""

import abc
import collections
import enum
import re
import statistics

import m2a_log_analyser.record


class ColumnError(Exception):
    """Base exception for all column errors."""


class ColumnUnrecognisedError(ColumnError):
    """Non error for when a column parser doesn't recognise an ID."""


class ColumnSyntaxError(ColumnError):
    """Raised when a column ID cannot be parsed.

    Where :exc:`ColumnUnrecognisedError` provides a way to indicate
    that an ID is not implemented by a concrete column class, this
    exception indciates that the ID is recognised, but is otherwise
    invalid.
    """


class Column(abc.ABC):
    """Record set column.

    This base class provides a foundation to build record set columns
    from. Each column has a canonical ID (provided by :meth:`__str__`)
    which can be parsed by :meth:`from_string` to create an appropriate
    :class:`Column` instance.

    Column objects are hashable on their ID. Note, however, that
    columns also evaluate equivalently to their IDs as plain strings.
    """

    UNDEFINED = object()

    @abc.abstractmethod
    def __str__(self):
        """Canonical ID of the column."""
        raise NotImplementedError

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        if isinstance(other, (Column, str)):
            return str(other) == str(self)
        return NotImplemented

    @classmethod
    def parse(cls, string):
        """Parse a column ID.

        This will try using each of the concrete column implementation,
        in turn, to parse the given ID.

        :param string: Column ID to parse.
        :type string: str

        :raises ColumnUnrecognisedError: If the given column ID is not
            recognised by any implementations.
        :raises ColumnSyntaxError: If the given column ID is recognised
            but is otherwise invalid.

        :returns: Column appropriate for the given ID.
        :rtype: Column
        """
        for implementation in Column.__subclasses__():
            try:
                return implementation.from_string(string)
            except ColumnUnrecognisedError:
                pass
        raise ColumnUnrecognisedError

    @classmethod
    @abc.abstractmethod
    def from_string(self, string):
        """Attempt to parse a column ID.

        If the given column ID is recognised, this should parse the ID
        and return an appropriate instance of the column implementation.

        :param string: Column ID to parse.
        :type string: str

        :raises ColumnUnrecognisedError: If the given column ID is not
            recognised by the implementation.
        :raises ColumnSyntaxError: If the given column ID is recognised
            but is invalid.

        :returns: Instance of the column implementation.
        :rtype: Column
        """
        raise NotImplementedError

    @abc.abstractmethod
    def evaluate(self, records, scale):
        """Extract value from a record set.

        This will calculate a value for the column from the given
        record set, as such, the return value is dependent on the
        column implementation. However, if no value can be calculated
        for the record set, :attr:`UNDEFINED` should be returned
        instead.

        A scaling value is given with the record set. The scaling value
        will account for any error introduced during record sampling.
        Therefore the scale only needs to be used when calculating
        values for the whole record set, e.g. counters.


        The given record set may be empty.

        :param records: Record set to calculate a value for.
        :type records: m2a_log_analyser.record.RecordSet
        :param scale: Scaling to apply to correct error.
        :type scale: float

        :returns: Value calculated for the column.
        """
        raise NotImplementedError


class TimeColumn(Column):
    """Always returns end time of the record set."""

    def __str__(self):
        return "TIME"

    @classmethod
    def from_string(cls, string):
        if string != "TIME":
            raise ColumnUnrecognisedError
        return cls()

    def evaluate(self, records, _):
        return records.end


class DistributionColumn(Column):
    """Statistical summary of value distributions.

    Distribution columns extract a field from all of the records in
    the set being evaluated and then apply a function to them to reduce
    the distribution to a single value. For example, taking the mean
    of all of the response times in a record set.

    If a record in the set doesn't have a value for the desired field
    then it is ignored.

    All reducer functions expect at least one value (and some need two)
    in order to function. If this condition is not met, then undefined
    will be returned.
    """

    _fields = {  # name : attribute
        "RESPONSE_TIME": "time_response",
        "UPSTREAM_TIME": "time_upstream",
        "RESPONSE_LENGTH": "length",
    }
    _reducers = {  # name : function
        "MEAN": statistics.mean,
        "MEDIAN": statistics.median,
        "MODE": statistics.mode,
        "STDEV": statistics.stdev,
        "MAX": max,
        "MIN": min,
    }

    def __init__(self, field, statistic):
        self._field = field
        self._statistic = statistic
        self._attribute = self._fields[field]
        self._reduce = self._reducers[statistic]

    def __str__(self):
        return self._field + "_" + self._statistic

    @classmethod
    def from_string(cls, string):
        for name in cls._fields:
            name_prefix = name + "_"
            if string.startswith(name_prefix):
                name_statistic = string[len(name_prefix):]
                if name_statistic in cls._reducers:
                    return cls(name, name_statistic)
                raise ColumnSyntaxError(
                    "Bad distribution statistic {!r}".format(name_statistic))
        raise ColumnUnrecognisedError

    def evaluate(self, records, _):
        try:
            return self._reduce(self._map(records))
        except (ValueError, statistics.StatisticsError):
            return self.UNDEFINED

    def _map(self, records):
        for record in records:
            record_attribute = getattr(record, self._attribute)
            if record_attribute is not None:
                yield record_attribute


class RequestCounter:

    def __str__(self):
        return "REQUEST"

    @classmethod
    def from_string(cls, string):
        if string == "REQUEST":
            return cls()

    def count(self, records):
        return len(records)


class ResponseCounter:

    def __init__(self, status):
        self.status = status

    def __str__(self):
        status = m2a_log_analyser.record.HTTPStatus(self.status)
        status_string = str(status)
        if self.status in status.Category:
            status_string = status_string.replace("0", "X")
        return "RESPONSE_" + status_string

    @classmethod
    def from_string(cls, string):
        if string.startswith("RESPONSE_"):
            suffix = string[len("RESPONSE_"):]
            suffix_category = False
            if suffix.endswith("XX"):
                suffix = suffix.replace("X", "0")
                suffix_category = True
            try:
                status = m2a_log_analyser.record.HTTPStatus(suffix)
            except ValueError:
                raise ColumnSyntaxError(
                    "Bad response status {!r}".format(suffix))
            if suffix_category:
                status = status.category
            return cls(status)

    def count(self, records):
        count = 0
        for record in records:
            record_status = record.status
            if self.status in list(record.status.Category):
                record_status = record.status.category
            if self.status == record_status:
                count += 1
        return count


class CounterColumn(Column):

    class Statistic(enum.Enum):

        COUNT = "COUNT"
        RATE = "RATE"
        PERCENTAGE = "PERCENTAGE"

    _counters = [RequestCounter, ResponseCounter]

    def __init__(self, counter, statistic):
        self.counter = counter
        self.statistic = statistic

    def __str__(self):
        return str(self.counter) + "_" + self.statistic.value

    @classmethod
    def from_string(cls, string):
        string_counter, string_statistic = string.rsplit("_", 1)
        try:
            statistic = cls.Statistic(string_statistic)
        except ValueError:
            raise ColumnUnrecognisedError
        for counter in cls._counters:
            counter_instance = counter.from_string(string_counter)
            if counter_instance:
                return cls(counter_instance, statistic)
        raise ColumnUnrecognisedError

    def evaluate(self, records, scale):
        count = int(round(scale * self.counter.count(records)))
        count_all = int(round(scale * len(records)))
        return {
            self.Statistic.COUNT: count,
            self.Statistic.RATE: count / records.duration.total_seconds(),
            self.Statistic.PERCENTAGE: count / count_all,
        }[self.statistic]


class AttributeCounter:

    def __init__(self, attribute, value):
        self.attribute = attribute
        self.value = value

    def count(self, records):
        count = 0
        for record in records:
            record_attribute = getattr(record, self.attribute)
            if record_attribute == self.value:
                count += 1
        return count


class RankedCounterColumn(Column):

    _pattern = re.compile(r"""
        ^
        (?P<field>CLIENT|USER_AGENT)
        _\#(?P<rank>[1-9]\d*)
        (_(?P<counter>COUNT|RATE|PERCENTAGE))?
        $
    """, re.VERBOSE)
    _fields = {  # name : attribute
        "CLIENT": "client",
        "USER_AGENT": "client_agent",
    }

    def __init__(self, field, rank, counter):
        self.field = field
        self.rank = rank
        self.counter = counter

    def __str__(self):
        string = "{0.field}_#{0.rank}".format(self)
        if self.counter:
            string += "_{0.value}".format(self.counter)
        return string

    @classmethod
    def from_string(cls, string):
        match = cls._pattern.match(string)
        if not match:
            raise ColumnUnrecognisedError
        field = match.group("field")
        rank = int(match.group("rank"))
        counter = CounterColumn.Statistic(
            match.group("counter")) if match.group("counter") else None
        return cls(field, rank, counter)

    def evaluate(self, records, error):
        try:
            ranking = self._rank(records)  # TODO: cache this
            ranked, ranked_count = \
                ranking.most_common(self.rank)[self.rank - 1]
        except IndexError:
            return self.UNDEFINED
        if self.counter:
            counter = AttributeCounter(self._fields[self.field], ranked)
            return CounterColumn(
                counter, self.counter).evaluate(records, error)
        return ranked

    def _rank(self, records):
        counter = collections.Counter()
        attribute = self._fields[self.field]
        for record in records:
            record_attribute = getattr(record, attribute)
            if record_attribute is not None:
                counter.update([record_attribute])
        return counter
