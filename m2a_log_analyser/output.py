"""Export record sets."""

import abc
import collections
import contextlib
import datetime
import ipaddress
import logging
import select
import socket
import uuid

import click
import msgpack

import m2a_log_analyser.column
import m2a_log_analyser.query


log = logging.getLogger(__name__)


class Output(abc.ABC):
    """Output summarised record sets.

    Each output is configured with zero or more columns, as typically
    an output will operate column-wise -- output the columns as
    requested in an appropriate format. However this isn't strictly
    necessary as the whole record set is made available to the output
    as well.

    Outputs are context manager. The default enter and exit handlers
    do nothing. Unless an output truly needs to perform any clean up,
    then their implementation is optional.
    """

    def __init__(self, columns):
        self.columns = tuple(columns)

    def __enter__(self): pass
    def __exit__(self, error, type_, traceback): pass

    def publish(self, records, error):
        row = {}
        for column in self.columns:
            row[column] = column.evaluate(records, error)
        self.emit(row)

    @abc.abstractmethod
    def emit(self, row):
        raise NotImplementedError


class TableOutput(Output):
    """Human-friendly table on stdout.

    For every record set, a new row of white space separated column
    values will be printed to stdout. Headers for the table will be
    periodically re-emitted.

    Values may be rounded or truncated.
    """

    def __init__(self, columns):
        super().__init__(columns)
        self._column_width = {
            column: max(len(row) for row
                in self._format_header(column)) + 1 for column in self.columns}
        self._emitted = 0
        self._emitted_since_headers = float("inf")

    def _format_header(self, column):
        column_id = str(column)
        column_id = column_id.replace("RESPONSE_", "")
        column_id = column_id.replace("_PERCENTAGE", " %")
        if isinstance(column, m2a_log_analyser.column.DistributionColumn):
            column_id = column_id.replace("_MEAN", "")
        parts = column_id.split("_")
        return [parts[0], " ".join(parts[1:])]

    def _format_value(self, column, value):
        column_id = str(column)
        if value is column.UNDEFINED:
            return "-", str.center
        if column_id.endswith("_PERCENTAGE"):
            return "{:.2f}".format(value * 100), str.rjust
        if isinstance(value, float):
            return "{:.3f}".format(value), str.rjust
        if isinstance(value, int):
            return str(value), str.rjust
        if isinstance(value, ipaddress.IPv4Address):
            return str(value).ljust(15), str.center
        if isinstance(value, datetime.datetime):
            return value.strftime("%a %H:%M:%S"), str.center
        return str(value), str.ljust

    def _emit_headers(self):
        header_rows = 0
        headers_formatted = []  # [row, row, ...], ...
        for column in self.columns:
            column_header = self._format_header(column)
            column_width = max(self._column_width[column],
                               max(len(row) for row in column_header)) + 1
            header_rows = max(header_rows, len(column_header))
            headers_formatted.append(
                [row.ljust(column_width) for row in column_header])
        self._emitted_since_headers = 0
        for row in range(header_rows):
            click.echo(click.style("".join(
                formatted[row] for formatted in headers_formatted),
                    bold=True, reverse=True))

    def emit(self, row):
        row_formatted = []
        for column in self.columns:
            column_formatted, column_justification \
                = self._format_value(column, row.get(column))
            self._column_width[column] = max(
                self._column_width[column], len(column_formatted))
            column_width = self._column_width[column]
            row_formatted.append(
                column_justification(column_formatted, column_width) + " ")
        if self._emitted_since_headers > click.get_terminal_size()[1] / 2:
            self._emit_headers()
        self._emitted_since_headers += 1
        click.echo("".join(row_formatted))


class CSVOutput(Output):
    """Comma-separated output to stdout.

    For every record set, a new row of comma separated column values
    will be printed to stdout. Headers will be emitted before the first
    row.

    Values will be quoted where appropriate, but never rounded or
    truncated.
    """

    def __init__(self, columns):
        super().__init__(columns)
        self._headers = False

    def _format_value(self, value):
        if isinstance(value, float):
            value = repr(value)
        value_string = str(value)
        if "," in value_string:
            value_string = '"' + value_string + '"'
        return value_string

    def emit(self, row):
        if not self._headers:
            click.echo(",".join(
                str(column) for column in self.columns))
            self._headers = True
        values = []
        for column in self.columns:
            values.append(self._format_value(row[column]))
        click.echo(",".join(values))


class TopOutput(Output):
    """Top-style output to stdout.

    For each record set, the terminal is cleared and the values
    for the configured columns are printed, giving a snapshot of
    the last record set.
    """

    def emit(self, row):
        click.clear()
        column_width = max(len(str(column)) for column in self.columns)
        for column in self.columns:
            column_value = row[column]
            click.echo("{} {}".format(click.style(
                str(column).ljust(column_width), bold=True), column_value))


class SocketOutputClientError(Exception):
    """Raised for all errors from socket output clients."""


class SocketOutput:
    """Serve parsed filtered records over a socket.

    This is a special output that serves parsed and filtered records
    over a Unix domain socket. The output must be configured with the
    path to the socket it will listen on. The columns are ignored.

    When a record set is published, the output will accept all incoming
    connections and serialise all records to be sent to connected
    clients. All connection handling happens non-blockingly. However,
    as this connection handling is tied to the record set publication,
    it's advisable to use a small record set periodicity with socket
    output.

    All communication across connected sockets are MessagePack encoded.
    Records are serialised as an array containing the error scaling
    value and the record it self. The record itself is a structure
    aligns with :class:`m2a_log_analyser.record.Record`.

    Each client has a :class:`m2a_log_analyser.query.Query` associated
    with it which is used to filter the records it receives. By default,
    the query will filter out all records. A client can update the
    query by sending a MessagePack encoded string. If the requested
    query is invalid, the client will be disconnected.

    The server socket will only be created when the output context
    is entered. An attempt it made to remove any existing file with
    the same name.

    :param path: Path of the Unix domain socket.
    :type path: pathlib.Path
    """

    def __init__(self, _, path):
        self._path = path
        self._socket = None
        self._socket_poller = select.poll()
        self._clients = {}  # descriptor : client
        self._clients_poller = select.poll()

    def __enter__(self):
        try:
            log.info("Removing %s", self._path)
            self._path.unlink()
        except FileNotFoundError:
            pass
        self._socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self._socket_poller.register(self._socket)
        self._socket.bind(str(self._path))
        self._socket.listen(socket.SOMAXCONN)
        log.info("Serving log records on %s", self._path)

    def __exit__(self, error, type_, traceback):
        self._socket.close()
        for client_descriptor, client in self._clients.items():
            log.info("%s Disconnected: shutting down", client)
            client.disconnect()
            self._clients_poller.unregister(client_descriptor)
        self._socket = None
        self._clients.clear()

    def publish(self, records, error):
        """Handle client connections.

        This will emit all records in the given record set to any
        connected clients. Any pending connections are accepted and
        client sockets are read from/written to if they are ready.
        """
        for record in records:
            for client in self._clients.values():
                client.emit(record, error)
        self._accept()
        self._dispatch_clients()

    def _accept(self):
        """Accept pending connections."""
        for descriptor, event in self._socket_poller.poll(0):
            if event & select.POLLIN:
                client_socket, _ = self._socket.accept()
                client = _SocketOutputClient(client_socket)
                self._clients[int(client)] = client
                self._clients_poller.register(client_socket)
                log.info("%s: Connected", client)

    def _dispatch_clients(self):
        """Process client sockets.

        This will poll all client sockets to see if they're ready to
        be read or to send enqueued records. Alternatively, if the
        client socket is disconnected, it will be cleaned up.
        """
        for descriptor, event in self._clients_poller.poll(0):
            client = self._clients[descriptor]
            try:
                if event & select.POLLHUP:
                    raise SocketOutputClientError("Hung up")
                if event & select.POLLIN:
                    client.receive()
                if event & select.POLLOUT:
                    client.send()
            except SocketOutputClientError as error:
                log.info("%s: Disconnected: %s", client, error)
                client.disconnect()
                del self._clients[descriptor]
                self._clients_poller.unregister(descriptor)


class _SocketOutputClient:
    """Connected client for a socket output.

    Given a connected client socket, instances of this class will
    wrap the interaction with the client. Each client has a unique
    ID in addition to the associated file descriptor.

    .. note::

        All socket IO is blocking. However, the caller may look ahead
        to determine if the IO will block; e.g. via poll(2).
    """

    _DEFAULT_QUERY = m2a_log_analyser.query.Query.from_string("(NOT all)")

    def __init__(self, socket):
        self._id = str(uuid.uuid4())
        self._socket = socket
        self._query = self._DEFAULT_QUERY
        self._buffer_in = msgpack.Unpacker(encoding="utf-8")
        self._buffer_out = []

    def __str__(self):
        """Unique ID of the client."""
        return self._id

    def __int__(self):
        """File descriptor of the client socket."""
        return self._socket.fileno()

    def emit(self, record, error):
        """Emit a record to the connected client.

        Note, this only enqueues a record to be sent. Actual socket IO
        is only one by subsequent calls to :meth:`send`.
        """
        if self._query.evaluate(record):
            self._buffer_out.append(self._encode(record, error))

    def disconnect(self):
        """Close the client socket."""
        self._socket.close()

    def receive(self):
        """Receive data from the connected client.

        This will read data from the client socket and attempt to
        decode it as MessagePack. Each decoded MessagePack object will
        be treated as a request to update the client's record filter
        query.

        If a partial MessagePack object is recieved, it is buffered
        for completion by later :meth:`receive` calls.

        :raises SocketOutputClientError: If any received queries are
            not strings or otherwise cannot be parsed as a valid query.
        """
        self._buffer_in.feed(self._socket.recv(4096))
        for object_ in self._buffer_in:
            try:
                if not isinstance(object_, str):
                    raise ValueError("Query not a string")
                self._query = m2a_log_analyser.query.Query.from_string(object_)
                log.info("%s: Set query to '%s'", self, self._query)
            except (ValueError, m2a_log_analyser.query.QueryError):
                raise SocketOutputClientError(
                    "Client sent bad query: {!r}".format(object_))

    def send(self):
        """Send enqueued records to the connected client.

        If any records have been enqueued by :meth:`emit`, this will
        send as much of the internal buffer as possible to the client.
        Any unsent data will be re-enqueued to be sent with later calls
        to this method.

        If no records had been enqueued, then this does nothing.
        """
        if not self._buffer_out:
            return
        fragment = b"".join(self._buffer_out)
        fragment_sent = self._socket.send(fragment)
        fragment_remaining = fragment[fragment_sent:]
        self._buffer_out.clear()
        if fragment_remaining:
            self._buffer_out.append(fragment_remaining)

    def _encode(self, record, error):
        """Encode a record as MessagePack."""
        return msgpack.packb((error, (
            record.time.timestamp(),
            int(record.client) if record.client else None,
            record.client_agent,
            record.path,
            record.query,
            record.method.value if record.method else None,
            int(record.status) if record.status else None,
            record.length,
            record.time_response,
            record.time_upstream,
        )), use_bin_type=True)
