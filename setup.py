import sys

if sys.version_info < (3, 4):
    raise Exception("Python version must be ≥ 3.4")

import setuptools


def version():
    """Get the m2a_log_analyser package version.

    .. note::

        This is the cannonical package version. If needed at runtime,
        :data:`m2a_log_analyser.version` and :data:`m2a_log_analyser
        .version_info` should beused.
    """
    return "0.1.0"


setuptools.setup(
    name="m2a-log-analyser",
    version=version(),
    author="Oliver Ainsworth",
    author_email="oliver.ainsworth@m2amedia.tv",
    license="Proprietary",
    url="https://bitbucket.org/m2amedia/m2a-log-analyser/",
    description="Analyser for M2A HTTP logs.",
    packages=setuptools.find_packages(),
    entry_points={
        "console_scripts": [
            "m2a-log-analyser=m2a_log_analyser.__main__:_main",
        ],
    },
    install_requires=[
        "click >=6.7",
        "isodate ==0.6.0",
        "multidict >=4.3.0",
        "pyparsing >=2.2.0, <3.0",
        "python-dateutil >=2.7",
    ],
    extras_require={
        "development": [
            "pylint",
            "pytest",
            "pytest-cov",
        ],
    },
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "License :: Other/Proprietary License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
    ],
)
